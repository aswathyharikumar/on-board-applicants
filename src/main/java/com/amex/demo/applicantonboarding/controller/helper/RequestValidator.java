package com.amex.demo.applicantonboarding.controller.helper;

import com.amex.demo.applicantonboarding.exception.InvalidRequestException;
import com.amex.demo.applicantonboarding.model.Applicant;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class RequestValidator {

    public void requestValidations(Applicant applicant) throws InvalidRequestException {

        if (applicant.getFirstName() == null || applicant.getFirstName().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter First Name");
        }

        if (applicant.getLastName() == null || applicant.getLastName().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter Last Name");
        }

        if (applicant.getJobStatus() == null || applicant.getJobStatus().toString().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter Job Status");
        }

        if (applicant.getMothersMaidenName() == null || applicant.getMothersMaidenName().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter mother's maiden name");
        }

        if (applicant.getAnnualIncome() < 0 ) {
            throw new InvalidRequestException("Invalid Request, Enter Annual Income");
        }

        if (applicant.getDateOfBirth() == null || applicant.getDateOfBirth().toString().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter Date of birth");
        }

        if (applicant.getResidentialStatus() == null || applicant.getResidentialStatus().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter Residential status");
        }

        if (applicant.getAddress() == null || applicant.getAddress().toString().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Enter Address");
        }

        if (!emailValidator(applicant.getEmailAddress()) || applicant.getEmailAddress().isEmpty()) {
            throw new InvalidRequestException("Invalid Request, Incorrect email ID format");
        }

        if (!(applicant.getJobStatus().equalsIgnoreCase("OTHER") ||
              applicant.getJobStatus().equalsIgnoreCase("PART_TIME_EMPLOYED") ||
              applicant.getJobStatus().equalsIgnoreCase("UNEMPLOYED") ||
              applicant.getJobStatus().equalsIgnoreCase("FULL_TIME_EMPLOYED") ||
              applicant.getJobStatus().equalsIgnoreCase("SELF_EMPLOYED") ||
              applicant.getJobStatus().equalsIgnoreCase("RETIRED"))) {
            throw new InvalidRequestException("Invalid Request. Please choose a valid job status from the list OTHER, PART_TIME_EMPLOYED, UNEMPLOYED, RETIRED, FULL_TIME_EMPLOYED, SELF_EMPLOYED");
        }

            if (!(applicant.getResidentialStatus().equalsIgnoreCase("OWNER") ||
                  applicant.getResidentialStatus().equalsIgnoreCase("TENANT") ||
                  applicant.getResidentialStatus().equalsIgnoreCase("OTHER"))) {
                throw new InvalidRequestException("Invalid Request. Please choose a valid residential status from the list OWNER, TENANT, OTHER");
            }

        }

    public void updateRequestValidations(Applicant applicant) throws InvalidRequestException {

        if (!(applicant.getApplicantStatus().name().equals("APPROVED") ||
                applicant.getApplicantStatus().name().equals("PENDING") ||
                applicant.getApplicantStatus().name().equals("DECLINED")))

            throw new InvalidRequestException("Invalid Request. Please choose a valid applicant status from the" +
                    " list APPROVED, DECLINED OR PENDING");
        }



    public boolean emailValidator(String email){

        String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(email);

        return matcher.matches();
    }
}
