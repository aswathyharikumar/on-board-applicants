package com.amex.demo.applicantonboarding.controller;

import com.amex.demo.applicantonboarding.controller.helper.RequestValidator;
import com.amex.demo.applicantonboarding.exception.ApplicantAlreadyExistsException;
import com.amex.demo.applicantonboarding.exception.ApplicantNotFoundException;
import com.amex.demo.applicantonboarding.exception.ApplicationAlreadyDeclined;
import com.amex.demo.applicantonboarding.exception.InvalidRequestException;
import com.amex.demo.applicantonboarding.model.Applicant;
import com.amex.demo.applicantonboarding.model.ApplicantStatus;
import com.amex.demo.applicantonboarding.service.ApplicantOnboardingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/onboard")
public class ApplicantOnboardingController {

@Autowired
private ApplicantOnboardingService applicantOnboardingService;

@Autowired
private RequestValidator requestValidator;

@GetMapping(value = "/applicants")
public List<Applicant> getApplicants(final @RequestParam(value = "status", required = false) ApplicantStatus status){
    return applicantOnboardingService.getApplicantDetails(status);
}

@GetMapping(value = "/applicants/{id}")
public Applicant getApplicantById(@PathVariable int id) throws ApplicantNotFoundException {
    return applicantOnboardingService.getApplicantById(id);
}

@PostMapping(value = "/applicants")
public Applicant createApplicant(@RequestBody Applicant applicant) throws InvalidRequestException, ApplicantAlreadyExistsException {
    Applicant candidateApplicant = null;

    requestValidator.requestValidations(applicant);
    if(applicantOnboardingService.checkIfApplicantExists(applicant) != null){
        throw new ApplicantAlreadyExistsException("Applicant already exists. Status is:" + applicantOnboardingService.checkIfApplicantExists(applicant).getApplicantStatus() + ".Applicant ID:" + applicantOnboardingService.checkIfApplicantExists(applicant).getApplicantIdentifier());
    }
    candidateApplicant =  applicantOnboardingService.createApplicant(applicant);
    return candidateApplicant;
}

@PutMapping(value = "/applicants/{id}")
public Applicant updateApplicant(@PathVariable int id,@RequestBody Applicant applicant) throws ApplicantNotFoundException, InvalidRequestException, ApplicationAlreadyDeclined {
    requestValidator.requestValidations(applicant);
    requestValidator.updateRequestValidations(applicant);

    return applicantOnboardingService.updateApplicant(id, applicant);
}

}


