package com.amex.demo.applicantonboarding.exception;

public class ApplicantAlreadyExistsException extends Exception {
    public ApplicantAlreadyExistsException() {
    }

    public ApplicantAlreadyExistsException(String message) {
        super(message);
    }

    public ApplicantAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicantAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public ApplicantAlreadyExistsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
