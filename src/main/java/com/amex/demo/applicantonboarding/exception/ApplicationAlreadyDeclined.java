package com.amex.demo.applicantonboarding.exception;

public class ApplicationAlreadyDeclined extends Exception {
    public ApplicationAlreadyDeclined() {
    }

    public ApplicationAlreadyDeclined(String message) {
        super(message);
    }

    public ApplicationAlreadyDeclined(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationAlreadyDeclined(Throwable cause) {
        super(cause);
    }

    public ApplicationAlreadyDeclined(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
