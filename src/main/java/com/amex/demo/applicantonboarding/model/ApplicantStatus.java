package com.amex.demo.applicantonboarding.model;

public enum ApplicantStatus {

    APPROVED,
    DECLINED,
    PENDING;
}
