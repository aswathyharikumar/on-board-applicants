package com.amex.demo.applicantonboarding.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table (name = "applicant")
//@SecondaryTable(name = "address", pkJoinColumns = @PrimaryKeyJoinColumn(name = "applicantId", referencedColumnName = "applicantIdentifier"))
public class Applicant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPLICANT_ID", updatable = false, nullable = false)
    private int applicantIdentifier;

    private String title;

    private String firstName;

    private String lastName;

    public String mothersMaidenName;

    private LocalDate dateOfBirth;

    private String jobStatus;

    private String jobDescription;

    private float annualIncome;

    private String emailAddress;

    private String residentialStatus;

    private String phoneNumber;

    @OneToOne(optional = false, cascade = {CascadeType.ALL})
    @JoinColumn(name = "APPLICANT_ID")
    private Address address;

    @Enumerated(EnumType.STRING)
    private ApplicantStatus applicantStatus;

    private String statusMessage;

    private LocalDateTime  applicationDate;

    public Applicant(String title, String firstName, String lastName, String mothersMaidenName, LocalDate dateOfBirth, String jobStatus, String jobDescription, float annualIncome, String emailAddress, String residentialStatus, String phoneNumber, Address address, ApplicantStatus applicantStatus, String statusMessage, LocalDateTime applicationDate) {
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mothersMaidenName = mothersMaidenName;
        this.dateOfBirth = dateOfBirth;
        this.jobStatus = jobStatus;
        this.jobDescription = jobDescription;
        this.annualIncome = annualIncome;
        this.emailAddress = emailAddress;
        this.residentialStatus = residentialStatus;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.applicantStatus = applicantStatus;
        this.statusMessage = statusMessage;
        this.applicationDate = applicationDate;
    }

    public Applicant() { }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public float getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(float annualIncome) {
        this.annualIncome = annualIncome;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getResidentialStatus() {
        return residentialStatus;
    }

    public void setResidentialStatus(String residentialStatus) {
        this.residentialStatus = residentialStatus;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ApplicantStatus getApplicantStatus() {
        return applicantStatus;
    }

    public void setApplicantStatus(ApplicantStatus applicantStatus) {
        this.applicantStatus = applicantStatus;
    }

    public int getApplicantIdentifier() {
        return applicantIdentifier;
    }

    public void setApplicantIdentifier(int applicantIdentifier) {
        this.applicantIdentifier = applicantIdentifier;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public LocalDateTime getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(LocalDateTime applicationDate) {
        this.applicationDate = applicationDate;
    }
}
