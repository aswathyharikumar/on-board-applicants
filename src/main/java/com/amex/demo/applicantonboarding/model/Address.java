package com.amex.demo.applicantonboarding.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Address {

    @Id
    @Column(name = "APPLICANT_ADDRESS_ID")
    int applicantAddressId;
    String buildingNumber;
    String streetName;
    String postCode;

    public Address(String buildingNumber, String streetName, String postCode) {
        this.buildingNumber = buildingNumber;
        this.streetName = streetName;
        this.postCode = postCode;
    }

    public Address() {
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
