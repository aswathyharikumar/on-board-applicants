package com.amex.demo.applicantonboarding.repository;

import com.amex.demo.applicantonboarding.model.Applicant;
import com.amex.demo.applicantonboarding.model.ApplicantStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface ApplicantOnboardingRepository extends JpaRepository<Applicant,Integer> {

    public List<Applicant> findByApplicantStatus(ApplicantStatus applicantStatus);
    public List<Applicant> findByFirstNameLike(String firstName);

    @Query("SELECT a FROM Applicant a WHERE a.firstName = :firstName and a.lastName = :lastName and a.mothersMaidenName = :mothersMaidenName and a.dateOfBirth = :dateOfBirth")
    public Applicant find(@Param("firstName") String firstName,
                          @Param("lastName") String lastName,
                          @Param("mothersMaidenName") String mothersMaidenName,
                          @Param("dateOfBirth") LocalDate dateOfBirth);

}
