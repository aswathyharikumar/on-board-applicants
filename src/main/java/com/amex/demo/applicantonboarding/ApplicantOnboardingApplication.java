package com.amex.demo.applicantonboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicantOnboardingApplication {

    public static void main(String[] args) {

        SpringApplication.run(ApplicantOnboardingApplication.class, args);
    }
}
