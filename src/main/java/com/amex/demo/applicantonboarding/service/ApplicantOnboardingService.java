package com.amex.demo.applicantonboarding.service;

import com.amex.demo.applicantonboarding.exception.ApplicantNotFoundException;
import com.amex.demo.applicantonboarding.exception.ApplicationAlreadyDeclined;
import com.amex.demo.applicantonboarding.repository.ApplicantOnboardingRepository;
import com.amex.demo.applicantonboarding.model.Applicant;
import com.amex.demo.applicantonboarding.model.ApplicantStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class ApplicantOnboardingService {

    @Autowired
    private ApplicantOnboardingRepository applicantOnboardingRepository;

    public List<Applicant> getApplicantDetails(ApplicantStatus applicantStatus){
        if(applicantStatus != null){
            return applicantOnboardingRepository.findByApplicantStatus(applicantStatus);
        } else {
            return applicantOnboardingRepository.findAll();
        }
    }

    public Applicant getApplicantById(int id) throws ApplicantNotFoundException {
        Optional<Applicant> applicant = applicantOnboardingRepository.findById(id);
        if(applicant.isPresent()){
            return applicant.get();
        } else {
            throw new ApplicantNotFoundException("Applicant not found. Please try with a valid applicant identifier");
        }
    }

    public Applicant createApplicant(Applicant applicant) {
       applicant.setApplicantIdentifier(0);
       applicant.setStatusMessage(null);
       applicant.setApplicantStatus(null);
       applicant.setApplicationDate(LocalDateTime.now());

       return applicantOnboardingRepository.save(checkEligibility(applicant));
    }

    public Applicant checkEligibility(Applicant applicant) {

        applicant.setApplicantStatus(ApplicantStatus.APPROVED);
        applicant.setStatusMessage("Application has been approved.");

        if(getAge(applicant.getDateOfBirth().getYear(), applicant.getDateOfBirth().getMonthValue(), applicant.getDateOfBirth().getDayOfMonth()) < 18){
            applicant.setApplicantStatus(ApplicantStatus.DECLINED);
            applicant.setStatusMessage("Application declined. Minimum age required for application is 18");
        } else if(applicant.getAnnualIncome() < 10000){
            applicant.setApplicantStatus(ApplicantStatus.DECLINED);
            applicant.setStatusMessage("Application declined. Minimum annual salary required for application is 10000");
        } else if(applicant.getJobStatus().equalsIgnoreCase("OTHER") && applicant.getAnnualIncome() > 10000){
            if(applicant.getJobDescription() == null) {
                applicant.setApplicantStatus(ApplicantStatus.DECLINED);
                applicant.setStatusMessage("Application declined. Job description mandatory when job status is OTHER");
            } else {
                applicant.setApplicantStatus(ApplicantStatus.PENDING);
                applicant.setStatusMessage("Application is pending for approval. This is forwarded to our customer care team.");
            }
        }
        return applicant;
    }

    public Applicant checkIfApplicantExists(Applicant applicant){

       Applicant existingApplicant = null;

       existingApplicant = applicantOnboardingRepository.find(applicant.getFirstName(), applicant.getLastName(), applicant.getMothersMaidenName(), applicant.getDateOfBirth());
       if(existingApplicant != null){
            return existingApplicant;
       }
       return existingApplicant;
    }

    public int getAge(int birthYear, int birthMonth, int birthDay){

        LocalDate fullBirthday = LocalDate.of(birthYear, birthMonth, birthDay);
        LocalDate now = LocalDate.now();
        long daysSinceBirth = now.toEpochDay() - fullBirthday.toEpochDay();

        return (int) (daysSinceBirth/365);
    }

    public Applicant updateApplicant(int id, Applicant applicant) throws ApplicantNotFoundException, ApplicationAlreadyDeclined {
        Optional<Applicant> updatableApplicant = applicantOnboardingRepository.findById(id);
        Applicant updatedApplicant = null;
        if(updatableApplicant.isPresent()){
            applicant.setApplicantIdentifier(id);
            if(updatableApplicant.get().getApplicantStatus().name().equals("DECLINED")){
            if(updatableApplicant.get().getApplicationDate().toEpochSecond(ZoneOffset.UTC) - LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) > 86400) {
                updatedApplicant = applicantOnboardingRepository.save(applicant);
            }
            else{
                throw new ApplicationAlreadyDeclined("Application is already declined. Try alteast 24 hours after application creation. Time left for retry:"
                + (ZonedDateTime.now().toEpochSecond() - updatableApplicant.get().getApplicationDate().toEpochSecond(ZoneOffset.UTC))/60/60 + " hours");
            }
            }else if(!updatableApplicant.get().getApplicantStatus().name().equals("DECLINED")){
                updatedApplicant = applicantOnboardingRepository.save(applicant);
            }
        } else {
            throw new ApplicantNotFoundException("Update failed, cannot find the specified customer.");
        }
        return updatedApplicant;
    }
}
